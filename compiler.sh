#!/bin/bash

MTA_COMPILER_URL="http://luac.mtasa.com/index.php"
OBFUSCATION_LEVEL=3
DEBUG_LEVEL=0
PUT_DATE=true
TARGET_RESOURCE_PATH="all"

now=$(date)
timestamp=$(date +%s)

usage()
{
    # Usage: ./compiler.sh [ -c ] [ -d ] [ -t ] [ -r ]
    #
    # Reference: https://wiki.multitheftauto.com/wiki/Lua_compilation_API
    # -h: Display help message (this thing)
    # -c: Obfuscation level, may between 0 and 3. Default is 3
    # -d: Enable debug option. Default is false
    # -t: Put current date to the compiled resource's meta.xml. Default is true
    # -r: Target resource path. Path/name of the resource to compile. Default is 'all'

    echo "Usage: $0 [ -h ] [ -c ] [ -d ] [ -t ] [ -r ]"

    echo;

    echo "Reference: https://wiki.multitheftauto.com/wiki/Lua_compilation_API"
    echo "-h: Display help message (this thing)"
    echo "-c: Obfuscation level, may between 0 and 3. Default is 3"
    echo "-d: Enable debug option. Default is false"
    echo "-t: Put current date to the compiled resource's meta.xml. Default is true"
    echo "-r: Target resource path. Path/name of the resource to compile. Default is 'all'"
};

# Option parsing bla bla
while getopts "c:dtr:h" opt; do
    case "${opt}" in
        # obfuscation level
        c)
            if [ "$OPTARG" -ge 0 ] && [ "$OPTARG" -le 3 ]
            then
                OBFUSCATION_LEVEL=[$OPTARG]
            else
                echo "Invalid argument for \`-c\`. Using default [ $OBFUSCATION_LEVEL ]"
            fi
            ;;

        d)
            DEBUG_LEVEL=1
            ;;

        t)
            PUT_DATE=true
            ;;

        # target resource name
        r)
            if [ ! -d "$OPTARG" ]
            then
                echo "ERROR: No directory found with path: \`$OPTARG\`"
                exit 1
                # return
            fi
            TARGET_RESOURCE_PATH=$OPTARG

            # if there is already a directory with the same name
            if [ -d "compiled/$TARGET_RESOURCE_PATH" ]
            then
                echo "WARNING: There is already a directory with the same name in the compiled directory! \`$TARGET_RESOURCE_PATH\`"
                echo "Do you want to continue to compilation? [Will delete old directory!]"
                echo "((y)es/(N)o/(m)ove)"
                echo -n "> "

                read -r decision
                : "${decision:=n}"

                if [ "${decision,,}" == "y" ]
                then
                    echo "Deleting old compiled resource..."
                    rm -r "compiled/$TARGET_RESOURCE_PATH"

                elif [ "${decision,,}" == "n" ]; then
                    echo "Terminating..."
                    exit 1

                elif [ "${decision,,}" == "m" ]; then
                    echo "Moving: Target path for moving old compiled resource (default is: $TARGET_RESOURCE_PATH-$timestamp)"
                    echo -n "> "
                    read -r newdirpath

                    : "${newdirname:="$TARGET_RESOURCE_PATH-$timestamp"}"
                    mkdir -p "compiled/$newdirpath"
                    mv "compiled/$TARGET_RESOURCE_PATH" "compiled/$newdirpath"

                    echo "Moved from \`compiled/$TARGET_RESOURCE_PATH\` into \`compiled/$newdirpath\`"
                fi
            fi
            ;;

        h)
            usage
            exit 1
            ;;

        *)
            echo "Unknown option: $opt"
            usage
            ;;
    esac
done

# Display a bunch of useless text about current compilation
info()
{
    # [ -c ] Compile Level: %d
    # [ -d ] Debug Enabled: true
    # [ -t ] Put Date     : %s
    # [ -r ] Target       : %s

    echo;

    echo "[ -c ] Compile Level: $OBFUSCATION_LEVEL"

    [ $DEBUG_LEVEL ] && echo "[ -d ] Debug Enabled: true"
    [ $PUT_DATE ]    && echo "[ -t ] Put Date     : $now"

    echo "[ -r ] Target       : \`$TARGET_RESOURCE_PATH\`"

    echo;
}

compile_script()
{
    echo "Compiling: $1"

    curl -s -X POST -F obfuscate="$OBFUSCATION_LEVEL" -F debug="$DEBUG_LEVEL" -F compile=1 -F luasource="@$1" $MTA_COMPILER_URL > "compiled/$1"
}

compile_scripts_in_dir()
{
    mkdir -p "compiled/$1"

    entries=$(ls "$1")
    for entry in $entries
    do
        fullpath="$1/$entry"

        # if it's a dir, then recall self
        if [[ -d "$fullpath" ]]
        then
            compile_scripts_in_dir "$fullpath"
        
        # or it's a file and it's extension is lua, then compile
        elif [[ -f "$fullpath" ]] && [[ "$fullpath" == *lua ]]
        then
            compile_script "$fullpath"
        fi
    done
}

compile_resource_scripts()
{
    compile_scripts_in_dir "$1"

    # copy meta.xml and put the date if enabled
    cp "$1/meta.xml" "compiled/$1"

    if [ $PUT_DATE == true ]
    then
        echo -e "\n<!-- Last compile date: $now -->" >> "compiled/$1/meta.xml"
    fi

    echo "Compilation is done..."
}

search_dir_for_resources()
{
    entries=$(ls "$1")
    for entry in $entries
    do
        fullpath="$1/$entry"
        if [[ -d $fullpath ]]
        then
            # if it's a group directory like [admin]
            if [[ $fullpath == \.\/\[*\] ]]
            then
                search_dir_for_resources "$fullpath"

            # or it has a meta.xml like a normal resource
            elif [[ -f "$fullpath/meta.xml" ]]
            then
                compile_resource_scripts "$fullpath"
            fi
        fi
    done
}

info

if [[ $TARGET_RESOURCE_PATH == "all" ]]
then
    search_dir_for_resources "."
else
    compile_resource_scripts "$TARGET_RESOURCE_PATH"
fi
