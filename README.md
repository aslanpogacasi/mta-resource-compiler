##### if you're on Windows, you'll need MinGW & Git Bash, probably you have it :^)

```
# Usage: ./compiler.sh [ -h ] [ -c ] [ -d ] [ -t ] [ -r ]
#
# Reference: https://wiki.multitheftauto.com/wiki/Lua_compilation_API
# -h: Display help message (this thing)
# -c: Obfuscation level, may between 0 and 3. Default is 3
# -d: Enable debug option. Default is false
# -t: Put current date to the compiled resource's meta.xml. Default is true: <!-- Last compile date: $now -->
# -r: Target resource path. Path/name of the resource to compile. Default is 'all'

# Examples;
$ ./compiler.sh -c 2 -d -t -r test_resource
$ ./compiler.sh -r path/to/resource
```

It will automatically create a folder named `compiled` and put all the compiled resources to in it (as you can think). So you can easily push your compiled scripts to your server, or you can use it with a GitLab/GitHub Webhook (would be cool i think).

### TODOs
- [ ] Error handling on compilation: Terminate process if any compilation fails and delete all compiled files
- [ ] Group directory support for `-r`: `./compiler.sh -r [admin]`
